#include "peripherals.h"
#define _SUPPRESS_PLIB_WARNING

//Start ISR timer
void START_ISR_TIMER(int start) {
    CloseTimer1();
    OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_256, start);
    // set up the timer interrupt with a priority of 2
    ConfigIntTimer1(T1_INT_ON | T1_INT_PRIOR_2);
    // enable multi-vector interrupts
    INTEnableSystemMultiVectoredInt();
}

//Start utility timer (motor time)
void START_UTILITY_TIMER(int start) {
    CloseTimer4();
    OpenTimer4(T4_ON | T4_SOURCE_INT | T4_PS_1_256, T4_TICK(start));
    ConfigIntTimer4(T4_INT_ON | T4_INT_PRIOR_2);
}
    
//OC3:
/*
 Takes: TIMER 2, OC3, RPB14 (pin 25)*/
void PWM_SETUP_RIGHT(int start) {
    OC3CONSET = 0; // disable output compare for setup
    OC3CONbits.OCM = 6; // enable PWM mode
    PR2 = PR_VAL;
    OC3RS = (PR2 + 1) * DUTY_EQ(start);
    T2CONSET = 0x8000; // Timer 2 enable, 1:1 prescale
    
    //RPB14 = 0x0005; // PWM1 pps on RB14 (pin 25)
    PPSUnLock;
    PPSOutput(4, RPB14, OC3);
    
   OC3CONSET = 0x8000; // enable output compare
   mPORTBSetPinsDigitalOut(BIT_6); //forward
   mPORTBSetPinsDigitalOut(BIT_7); //reverse
}

//OC1:
//Takes: TIMER 3, OC1, RPB3 (pin 7))
void PWM_SETUP_LEFT(int start) {
    OC1CONSET = 0; // disable output compare for setup
    OC1CONbits.OCM = 6; // enable PWM mode
    PR3 = PR_VAL;
    OC1RS = (PR3 + 1) * DUTY_EQ(start);
    T3CONSET = 0x8000; // Timer 3 enable, 1:1 prescale
    
    PPSUnLock;
    PPSOutput(1, RPB3, OC1);
    
    OC1CONSET = 0x8000; // enable output compare
    mPORTASetPinsDigitalOut(BIT_4); //Forward
    mPORTBSetPinsDigitalOut(BIT_5); //Reverse
}

//Enable ADC
void initADC(unsigned adcPINS, unsigned numPins) {  
    AD1CON1 = 0x0000; // disable ADC
    
    mPORTBSetPinsAnalogIn(BIT_0 | BIT_1 | BIT_2 | BIT_13 | BIT_15); //B0 and B15 analog input
    mPORTASetPinsAnalogIn(BIT_0 | BIT_1);
 
    // AD1CON1<2>, ASAM    : Sampling begins immediately after last conversion completes
    // AD1CON1<7:5>, SSRC  : Internal counter ends sampling and starts conversion (auto convert)
    AD1CON1SET = 0x00e4;
 
    // AD1CON2<1>, BUFM    : Buffer configured as one 16-word buffers
    // AD1CON2<10>, CSCNA  : Scan inputs
    AD1CON2 = 0x0400;
 
    // AD2CON2<5:2>, SMPI  : Interrupt flag set at after numPins completed conversions
    AD1CON2SET = (numPins-1) << 2;
 
    // AD1CON3<7:0>, ADCS  : TAD = TPB * 2 * (ADCS<7:0> + 1) = 4 * TPB in this example
    // AD1CON3<12:8>, SAMC : Acquisition time = AD1CON3<12:8> * TAD = 15 * TAD in this example
    AD1CON3 = 0x0fff;//AD1CON3 = 0x0f01;
 
    // AD1CHS is ignored in scan mode
    AD1CHS = 0;
 
    // select which pins to use for scan mode
    AD1CSSL = adcPINS;
    
    AD1CON1SET = 0x8000;   
    /*
    CloseADC10();
    OpenADC10(ADC_PARAM1, ADC_PARAM2, ADC_PARAM3, ADC_PARAM4, ADC_PARAM5);
    EnableADC10();
    
    AcquireADC10();
     * */
}
