#ifndef SQUAREUP_H
#define SQUAREUP_H
#include "peripherals.h"
//#define DEBUG_STARTER
//#define DEBUG_LINE
#define DEBUG_SHARP

/***********************
 * STATE MACHINE
 */
enum STATES {
    STANDBY,
    ERRORCHK,
    INIT_1,
    INIT_2,
    OG_1_1,
    OG_1_2,
    DETECT_R_S,
    DETECT_L_S,
    SEARCH_2,
    DEFEND,
    LOCKED,
    DETECT_L,
    DETECT_R,
    SEARCH,
    WHITE
};
enum STATES state;

/*********************
 * Wait for start
 */
#define NOT_STARTED mPORTBReadBits(BIT_8)
#define SETUP_STARTER mPORTBSetPinsDigitalIn(BIT_8)
int initTime;
int startPulse;
int waitForStart();
int accel;

/********************
 * Start initialization process
 */ 
void squareUp();

/********************
 * Movement utility
 */

unsigned int frontRightLine, frontLeftLine, rightSharp, rearLine, frontSharp2, rearSharp2, leftSharp;
int l_spd, r_spd;
int max_spd;
void forward(int l_str, int r_str);
void reverse(int l_str, int r_str);
void turnRight(int l_str, int r_str);
void turnLeft(int l_str, int r_str);
#endif
