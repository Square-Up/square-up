#define _SUPPRESS_PLIB_WARNING
#include "config.h"
#include "SquareUp.h"
int init_time = 10;
int init_samples = 6000;
void main() {
    SYSTEMConfigPerformance(SYS_FRQ);
    accel = 100;
    max_spd = 100;
    while(waitForStart() != 0) {
        //NOTHING
    }
    
    err_r = 3000;
    err_l = 3000;
    squareUp();
    while(1) {
        
        
        /*****************SHARP LOGIC HANDLER************/
        if(rightSharp >  400)//240) 
            if (err_r > 0 ) err_r--;
            else { d_r = 1; 
#ifdef DEBUG_SHARP
            mPORTBSetBits(BIT_9);
#endif
            err_r = 3000;}
        else
            if (err_r >0) err_r--;
        else {
            d_r = 0;
#ifdef DEBUG_SHARP
            mPORTBClearBits(BIT_9);
#endif
            err_r = 3000;
        }

        if (leftSharp > 400)//300)//560) 
            if (err_l > 0) err_l--;
            else {
                d_l = 1;
#ifdef DEBUG_SHARP
                mPORTBSetBits(BIT_4);
#endif
                err_l = 3000;
            } else
            if (err_l > 0) err_l--;
        else {
            d_l = 0;
#ifdef DEBUG_SHARP
            mPORTBClearBits(BIT_4);
#endif
            err_l = 3000;
        }


        if (init_time > 0) init_time--;

        /******************LINE LOGIC HANDLER**************/
        if (frontRightLine IN_WHITE_FRONTR || frontLeftLine IN_WHITE_FRONTL ){//|| rearLine IN_WHITE_REAR) {
            if (state == OG_1_1) {
                state = OG_1_2;
            } else {
                if (state != WHITE) accel = 75;
                state = WHITE;
                tick = 3;
            }
        }

        /*********************STATE MACHINE HANDLER**********/
        switch (state) {
            case STANDBY:
                break;
            case INIT_1:
                state = OG_1_1;
                break;
            case OG_1_1:
                reverse(accel,accel);
                if (d_r == 1 && d_r == 1) state = LOCKED;
                else reverse(accel,accel);
                break;
            case OG_1_2:
                forward(0,0);
                if (d_r == 1 && d_r == 1) state = LOCKED;
                break;
            case SEARCH_2:
                if(d_r == 1 && d_l == 1) { accel = 75; state = LOCKED; }
                else if (d_r == 1) { accel = 75; state = DETECT_R; }
                else if (d_l == 1) { accel = 75; state = DETECT_L; }
                turnRight(accel, accel);
                break;
            case LOCKED:
                //forward(accel, accel);
                forward(100, 100);
                if(d_r == 1 && d_l == 0) {accel = 100; state = DETECT_R_S;}
                if(d_r == 0 && d_l == 1) {accel = 100; state = DETECT_L_S;}
                break;
            case DETECT_R_S:
                forward(100, 75);
                break;
            case DETECT_L_S:
                forward(75,100);
                break;
            case DETECT_R:
                //forward(accel,accel-25);
                if(d_r == 1 && d_l == 1) { accel = 75; state = LOCKED; }
                else if (d_r == 1) { accel = 75; state = DETECT_R; }
                else if (d_l == 1) { accel = 75; state = DETECT_L; }
                else { tick = 3; state = SEARCH;} 
                turnRight(70, 70);
                break;
            case DETECT_L:
                //forward(accel-25,accel);
                if(d_r == 1 && d_l == 1) { accel = 75; state = LOCKED; }
                else if (d_r == 1) { accel = 75; state = DETECT_R; }
                else if (d_l == 1) { accel = 75; state = DETECT_L; }
                else { tick = 3; state = SEARCH;} 
                turnLeft(70, 70);
                break;
            case DEFEND:
                forward(0, 0);
                //mPORTBSetBits(BIT_9);
                //mPORTBSetBits(BIT_4);
                break;
            case SEARCH:
                forward(accel, accel);
                //mPORTBSetBits(BIT_9);
                break;
            case WHITE:
                //mPORTBClearBits(BIT_9);
                if (rearLine IN_WHITE_REAR) {
                    forward(accel, accel);
                } else if (frontRightLine IN_WHITE_FRONTR) {
                    reverse(accel - 25, accel);
                } else if (frontLeftLine IN_WHITE_FRONTL) {
                    reverse(accel, accel - 25);

                }
                break;
        }
#ifdef DEBUG_LINE
        if (frontRightLine IN_WHITE_FRONTR) mPORTBSetBits(BIT_9);
        else mPORTBClearBits(BIT_9);

        if (rearLine IN_WHITE_REAR) mPORTBSetBits(BIT_4);
        else mPORTBClearBits(BIT_4);
#endif
    }
    
}

void __ISR(_TIMER_1_VECTOR, IPL2SOFT) Timer1Handler(void) {
    mT1ClearIntFlag();
    if(state==ERRORCHK)
        if (startPulse > 0) startPulse--;
    if(state == STANDBY) 
            if(initTime >0 ) initTime--;
    
    
    
    if (state != ERRORCHK && state != STANDBY && init_time == 0) {
        /******************ADC ACCQUIRE*******************/
        while( ! IFS0bits.AD1IF);       // wait until buffers contain new samples
        AD1CON1bits.ASAM = 0;           // stop automatic sampling (essentially shut down ADC in this mode)
        IFS0CLR = 0x10000000;           // clear ADC interrupt flag
        
        frontRightLine = ADC1BUF0;
        frontLeftLine = ADC1BUF1;
        rightSharp = ADC1BUF6;
        leftSharp = ADC1BUF5;
        rearLine = ADC1BUF3;
        
        AD1CON1bits.ASAM = 1;           // restart automatic sampling 
    }
}

void __ISR(_TIMER_4_VECTOR, IPL2SOFT) Timer4Handler(void) {
    mT4ClearIntFlag(); 
    switch(state) {
        case STANDBY:
            break;
        case SEARCH:
            if (accel <= max_spd/*max_spd*/) {
                accel += 25;
            }
        case SEARCH_2:
            if(tick > 0) tick --;
            else { 
                tick = 5; 
                if(state==SEARCH) state = SEARCH_2;
                else state = SEARCH;
            }
            break;
        case LOCKED:
            break;
        case DETECT_R:
        case DETECT_L:
            if (accel <= max_spd/*max_spd*/) {
                accel += 25;
            }
            break;
        case WHITE:
            if(accel < max_spd) accel += 25;
            if(frontRightLine OUT_WHITE_FRONTR && frontLeftLine OUT_WHITE_FRONTL ) {//&& rearLine OUT_WHITE_REAR) {
                if(tick > 0) tick--;
                else {  accel = 75; tick = 10; state = SEARCH_2;}
            }
            break;
    }   
}
