#ifndef PERIPHERALS_H
#define PERIPHERALS_H
#include <xc.h>
#include <p32xxxx.h>
#include <plib.h>
#include <sys/attribs.h>

#define SYS_FRQ 			(40000000L)
#define PB_DIV         		1

/*******************************
 Timer Utility
 */
#define T1_PRESCALE       		256
#define TOGGLES_PER_SEC		1000
#define T1_TICK       		(SYS_FRQ/PB_DIV/T1_PRESCALE/TOGGLES_PER_SEC)
void START_ISR_TIMER(int start);

#define T4_PRESCALE       	256
#define T4_TICK(ms)       		(SYS_FRQ/PB_DIV/T4_PRESCALE/TOGGLES_PER_SEC)*ms
int tick;
void START_UTILITY_TIMER(int start);

/*********************************
 PWM UTILITY
 */
#define DUTY_EQ(d_cycle)            ((float)d_cycle/100)
#define MAX_PWM                     SYS_FRQ/FPB_DIV/SAMPLE_RATE     // eg 12800 at 3125hz.
#define PWM_FREQ                    24000
#define PR_VAL                      SYS_FRQ/PWM_FREQ
#define FPB_DIV                     8
#define SAMPLE_RATE                 3125                // 320us interval
#define WHITE_T_FRONTL                     50
#define IN_WHITE_FRONTL                 < WHITE_T_FRONTL
#define OUT_WHITE_FRONTL                   > WHITE_T_FRONTL
#define WHITE_T_FRONTR                     50
#define IN_WHITE_FRONTR                 < WHITE_T_FRONTR
#define OUT_WHITE_FRONTR                   > WHITE_T_FRONTR
#define WHITE_T_REAR                      55
#define IN_WHITE_REAR                   <WHITE_T_REAR
#define OUT_WHITE_REAR                  > WHITE_T_REAR

int tick;

/**********************************
  RIGHT DRIVE
 */
void PWM_SETUP_RIGHT(int start);
#define RIGHT_SET_DUTY(duty)        OC3RS = (PR2 + 1) * DUTY_EQ(duty)
#define RIGHT_BACKWARD              mPORTBSetBits(BIT_6); mPORTBClearBits(BIT_7)
#define RIGHT_FORWARD              mPORTBSetBits(BIT_7); mPORTBClearBits(BIT_6)

/**********************************
  LEFT DRIVE
 */
void PWM_SETUP_LEFT(int start);
#define LEFT_SET_DUTY(duty)         OC1RS = (PR3 + 1) * DUTY_EQ(duty)
#define LEFT_BACKWARD                mPORTASetBits(BIT_4); mPORTBClearBits(BIT_5)
#define LEFT_FORWARD               mPORTBSetBits(BIT_5); mPORTAClearBits(BIT_4)

/*********************************
 *ADC UTILITY
 */

//#define ADC_PARAM1  ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_MANUAL | ADC_AUTO_SAMPLING_OFF
#define ADC_PARAM1 ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON
#define ADC_PARAM2  ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_ON | ADC_SAMPLES_PER_INT_2 | ADC_ALT_BUF_OFF | ADC_ALT_INPUT_OFF
#define ADC_PARAM3  ADC_CONV_CLK_SYSTEM | ADC_SAMPLE_TIME_3 | ADC_CONV_CLK_16Tcy
#define ADC_PARAM4  ENABLE_AN0_ANA | ENABLE_AN1_ANA | ENABLE_AN2_ANA |  ENABLE_AN3_ANA | ENABLE_AN4_ANA | ENABLE_AN9_ANA | ENABLE_AN11_ANA
#define ADC_PARAM5 SKIP_SCAN_ALL & ~(SKIP_SCAN_AN2 | SKIP_SCAN_AN0 | SKIP_SCAN_AN1 | SKIP_SCAN_AN2 | SKIP_SCAN_AN4 | SKIP_SCAN_AN9 | SKIP_SCAN_AN11)
void initADC(unsigned, unsigned); // Init the ADC
int err_r, err_l;
int d_r, d_l;

#endif
