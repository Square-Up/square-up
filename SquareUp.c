#include "SquareUp.h"
#define _SUPPRESS_PLIB_WARNING


/***********************************
 MOVEMENT
 ***********************************/
int waitForStart() {
    //set up starter
    initTime = 300;
    startPulse = 150;
    state = STANDBY;

    START_ISR_TIMER(1);
    
    mPORTBSetPinsDigitalOut(BIT_4);
    mPORTBSetPinsDigitalOut(BIT_9);
    SETUP_STARTER;
    
    while(initTime);
    
    mPORTBClearBits(BIT_9);
    mPORTBClearBits(BIT_4);
    
    
    
    while (NOT_STARTED) {
        //Do nothing
    }
    
   mPORTBSetBits(BIT_9);
   
    //Error check signal
   state = ERRORCHK;
   while(!NOT_STARTED) {
       //Do nothing
   }
   
   
   
   //Return 1 if debounce
   if (startPulse != 0) {
       state = STANDBY;
       return 1;
   }
   else {
       mPORTBSetBits(BIT_4);
       state = INIT_1;
       return 0;
   }
}

/***********************************
 INITIALIZATION
 ***********************************/
void squareUp() {
    WritePeriod1(T1_TICK);
    
    PWM_SETUP_LEFT(0);
    PWM_SETUP_RIGHT(0);
    
    START_UTILITY_TIMER(1000);
    initADC(0x0A1F, 7);
}


/***********************************
 MOVEMENT
 ***********************************/
void forward(int l_str, int r_str) {
    RIGHT_FORWARD;
    LEFT_FORWARD;
    
    LEFT_SET_DUTY(l_str);
    RIGHT_SET_DUTY(r_str);
}
void reverse(int l_str, int r_str) {
    RIGHT_BACKWARD;
    LEFT_BACKWARD;
    
    LEFT_SET_DUTY(l_str);
    RIGHT_SET_DUTY(r_str);
}
void turnRight(int l_str, int r_str) {
    RIGHT_BACKWARD;
    LEFT_FORWARD;
    
    LEFT_SET_DUTY(l_str);
    RIGHT_SET_DUTY(r_str);
}
void turnLeft(int l_str, int r_str){
    RIGHT_FORWARD;
    LEFT_BACKWARD;
    
    LEFT_SET_DUTY(l_str);
    RIGHT_SET_DUTY(r_str);
}
void updateSpeed() {
    /**
    if (req_l_spd > l_spd) l_spd++;
    else l_spd = req_l_spd;
    
    if (req_r_spd > r_spd) r_spd++;
    else r_spd = req_r_spd;
    
    LEFT_SET_DUTY(l_spd);
    RIGHT_SET_DUTY(r_spd);
     * */
}

